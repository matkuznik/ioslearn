//
//  CardGameViewController.m
//  Card Game
//
//  Created by Sure Case on 20/02/14.
//  Copyright (c) 2014 Sure Case. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "Card.h"
#import "MatchGameGame.h"

@interface CardGameViewController ()

@property (strong, nonatomic) Deck *deck;
@property (strong, nonatomic) MatchGameGame* game;

@end

@implementation CardGameViewController

- (MatchGameGame *) game{
    
    if(!_game){
        _game = [[MatchGameGame alloc] initWithCardCount:[self.cardButtons count] usingDeck:[self createDeck]];
        
    }
    return _game;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(bestScoreChange)
                                                 name:[MatchGameGame NOTYFICATION_NAME]
                                               object:self.game];
    
}

- (void) bestScoreChange{
    [self setBestScore];
}
- (void) setBestScore{
    self.bestScorLabel.text = [NSString stringWithFormat:@"Best score: %d", self.game.bestScore];
}
- (void)viewDidLoad{
    [super viewDidLoad];
    [self setBestScore];
    [self setMatchCardCounSwitcher];
}
- (void)setMatchCardCounSwitcher{
    [self.matchCardCountSwitcher setOn:self.game.cardMatchCount>2];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:[MatchGameGame NOTYFICATION_NAME]     	
                                                  object:self.game];
}
-(Deck *)createDeck{
    return [[PlayingCardDeck alloc] init];
}
- (IBAction)touch:(UIButton *)sender {
    
    int cardIndex = [self.cardButtons indexOfObject:sender];
    [self.game chooseCardAtIndex:cardIndex];
    [self updateUI];
}

- (void)updateUI{
    for (UIButton *cardButton in self.cardButtons) {
        int cardIndex = [self.cardButtons indexOfObject:cardButton];
        Card *card = [self.game cardAtIndex:cardIndex];
        [cardButton setTitle: [self titleForCard:card] forState:UIControlStateNormal];
        [cardButton setBackgroundImage: [self backgroundForCard:card]  forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
    }
    
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
    
    
    [self updateEarnLabel];
}

- (void)updateEarnLabel{
    if(self.game.earnedScore == 0) return;
    self.earnScoreLabel.text = [NSString stringWithFormat:self.game.earnedScore<0?@"You lost: %d":@"You earn: %d", self.game.earnedScore];
    NSMutableAttributedString *earnLabelText = [self.earnScoreLabel.attributedText mutableCopy];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict[NSFontAttributeName] = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    if(self.game.earnedScore > 0){
        dict[NSForegroundColorAttributeName] = [[UIColor greenColor] colorWithAlphaComponent:0.7];
    }else if(self.game.earnedScore < 0){
        dict[NSForegroundColorAttributeName] = [[UIColor redColor] colorWithAlphaComponent:0.7];
    }
    [earnLabelText setAttributes:dict range: [self.earnScoreLabel.text rangeOfString:self.earnScoreLabel.text] ];
    self.earnScoreLabel.attributedText = earnLabelText;
}

- (IBAction)cantMatchCard:(UISwitch *)sender {
   // NSLog(([sender isOn] ? @"Is Sender on? Yes" : @"Is Sender on? No") );
   self.game.cardMatchCount = [sender isOn]?3:2; //if isOn match 3 card
}



- (NSString *)titleForCard:(Card *)card{
    return card.isChoosen?card.contents:@"";
}
-(UIImage *)backgroundForCard:(Card *)card{
    return [UIImage imageNamed:card.isChoosen?@"frontCard":@"backCard"];
}
@end
