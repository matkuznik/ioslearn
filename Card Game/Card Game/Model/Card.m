//
//  Card.m
//  Card Game
//
//  Created by Sure Case on 20/02/14.
//  Copyright (c) 2014 Sure Case. All rights reserved.
//

#import "Card.h"


@implementation Card

- (int) match:(NSArray *)otherCard{
    
    int score = 0;
    
    for (Card *card in otherCard) {
        if([card.contents isEqualToString:self.contents]){
            score = 1;
        }
    }
    
    return score;
}
@end
