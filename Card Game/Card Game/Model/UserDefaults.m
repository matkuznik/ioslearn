//
//  UserDefaults.m
//  Card Game
//
//  Created by Sure Case on 21/02/14.
//  Copyright (c) 2014 Sure Case. All rights reserved.
//

#import "UserDefaults.h"

@implementation UserDefaults

static NSString *BEST_SCORE_KEY = @"bestScore";
static NSString *CARD_MACH_COUNT_KEY = @"cardMatchCount";

+(NSInteger)bestScore{
    return [[NSUserDefaults standardUserDefaults] integerForKey:BEST_SCORE_KEY];
}

+ (void)setBestScore:(NSInteger)bestScore{
    [[NSUserDefaults standardUserDefaults] setInteger:bestScore forKey:BEST_SCORE_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSInteger)matchCardCount{
    NSUInteger matchCardCount = [[NSUserDefaults standardUserDefaults] integerForKey:CARD_MACH_COUNT_KEY];
    if (matchCardCount < 2 || matchCardCount > 3) {
        matchCardCount = 2;
    }
    return matchCardCount;
}
+(void)setMatchCardCount:(NSInteger)matchCardCount{
    [[NSUserDefaults standardUserDefaults] setInteger:matchCardCount forKey:CARD_MACH_COUNT_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
