//
//  PlayingCardDeck.m
//  Card Game
//
//  Created by Sure Case on 20/02/14.
//  Copyright (c) 2014 Sure Case. All rights reserved.
//

#import "PlayingCardDeck.h"
#import "PlaingCard.h"
@implementation PlayingCardDeck

- (instancetype)init{
    
    self = [super init];
    
    if (self) {
        
        
        for (NSString *suit in [PlaingCard validSuits]) {
            for (NSUInteger rank = 1; rank <= [PlaingCard maxRank]; rank++) {
                PlaingCard *card = [[PlaingCard alloc] init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card];
            }
        }
    }
    
    return self;
}
@end
