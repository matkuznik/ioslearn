//
//  MatchGameGame.h
//  Card Game
//
//  Created by Sure Case on 21/02/14.
//  Copyright (c) 2014 Sure Case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"
@interface MatchGameGame : NSObject

- (instancetype)initWithCardCount:(NSUInteger)count usingDeck: (Deck *)deck;

- (void)chooseCardAtIndex:(NSUInteger)index;
- (Card *)cardAtIndex:(NSUInteger)index;
+ (NSString *) NOTYFICATION_NAME;
@property (nonatomic, readonly) NSInteger bestScore;
@property (nonatomic, readonly) NSInteger score;
@property (nonatomic, readonly) NSInteger earnedScore;
@property (nonatomic ) NSUInteger cardMatchCount;//how many card will be match

@end
