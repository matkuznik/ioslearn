//
//  Deck.h
//  Card Game
//
//  Created by Sure Case on 20/02/14.
//  Copyright (c) 2014 Sure Case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

- (void)addCard:(Card *)card atTop:(BOOL)atTop;

- (void)addCard:(Card *)card;

- (Card *)drawRandomCard;
@end
