//
//  UserDefaults.h
//  Card Game
//
//  Created by Sure Case on 21/02/14.
//  Copyright (c) 2014 Sure Case. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaults : NSObject

+(NSInteger)bestScore;
+(void)setBestScore:(NSInteger)bestScore;

+(NSInteger)matchCardCount;
+(void)setMatchCardCount:(NSInteger)matchCardCount;
@end
