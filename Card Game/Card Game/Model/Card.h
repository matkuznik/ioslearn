//
//  Card.h
//  Card Game
//
//  Created by Sure Case on 20/02/14.
//  Copyright (c) 2014 Sure Case. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject
@property (strong, nonatomic) NSString *contents;

@property (nonatomic, getter = isChoosen) BOOL choosen;
@property (nonatomic, getter = isMatched) BOOL matched;

- (int)match:(NSArray *)card;
@end
