//
//  PlaingCard.m
//  Card Game
//
//  Created by Sure Case on 20/02/14.
//  Copyright (c) 2014 Sure Case. All rights reserved.
//

#import "PlaingCard.h"

@implementation PlaingCard


- (NSString *)contents{
    NSArray *rankStrings = [PlaingCard rankStrings];
    return [rankStrings[self.rank] stringByAppendingString:self.suit];
}

@synthesize suit = _suit;

+ (NSArray *)validSuits{
    return @[@"♠️", @"♥️", @"♦️", @"♣️"];
}

+ (NSArray *)rankStrings{
    return @[@"?", @"A", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"J", @"Q", @"K"];
}

-(void)setSuit:(NSString *)suit{
    if( [ [PlaingCard validSuits] containsObject:suit]){
        _suit = suit;
    }
}

- (NSString *)suit {
    return _suit ? _suit:@"?";
}

+ (NSUInteger)maxRank{ return [[self rankStrings] count]-1;}

- (void)setRank:(NSUInteger)rank{
    if (rank <= [PlaingCard maxRank]) {
        _rank = rank;
    }
}

- (int)match:(NSArray *)cards{
    
    int score = 0;
    
    if([cards count] == 1){
        PlaingCard *otherCard = [cards firstObject];
        
        if ([otherCard.suit isEqualToString:self.suit]) {
            score = 1;
        }else if(otherCard.rank == self.rank){
            score = 4; 
        }
    }else{
        for (Card *otherCard in cards) {
            if (otherCard == self) continue;
            score += [self match:@[otherCard]];
        }
    }
    
    return score;
}
@end