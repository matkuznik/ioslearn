//
//  MatchGameGame.m
//  Card Game
//
//  Created by Sure Case on 21/02/14.
//  Copyright (c) 2014 Sure Case. All rights reserved.
//

#import "MatchGameGame.h"
#import "UserDefaults.h"

@interface MatchGameGame()
@property (nonatomic, readwrite) NSInteger score;
@property (nonatomic, readwrite) NSInteger bestScore;
@property (nonatomic, readwrite) NSInteger earnedScore;
@property (nonatomic, strong) NSMutableArray *cards; //of Card
@property (nonatomic, strong) NSMutableArray *choosenCards; //of Card
@end

@implementation MatchGameGame
static const int MISMATCH_PENALTY = 2;
static const int MATCH_BONUS = 2;
static const int COST_TO_FLIP = 1;

- (instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck{
    
    self = [super init];
    
    if(self){
        for(int i=0; i<count; i++){
            Card *card = [deck drawRandomCard];
            if(card){
                [self.cards addObject:card];
            }else{
                self = nil;
                break;
            }
        }
        self.cardMatchCount = [UserDefaults matchCardCount];
        
        self.bestScore = [UserDefaults bestScore];
    }
    return self;
}

-(instancetype) init{
    return nil;
}
+(NSString *)NOTYFICATION_NAME{
    return @"bestScoreNotification";
}
- (NSMutableArray *)cards{
    if(!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}
- (NSMutableArray *)choosenCards{
    if(!_choosenCards) _choosenCards = [[NSMutableArray alloc] init];
    return _choosenCards;
}
- (void)setCardMatchCount:(NSUInteger)cardMatchCount{
    _cardMatchCount = cardMatchCount;
    [UserDefaults setMatchCardCount:cardMatchCount];
}

- (void)chooseCardAtIndex:(NSUInteger)index{
    int score = self.score;
    Card *card = [self cardAtIndex:index];
    
    if(!card.isMatched){
        if(card.isChoosen){
            card.choosen = NO;
            [self.choosenCards removeObject:card];
        }else{
            [self.choosenCards addObject:card];
            if ([self.choosenCards count] >= self.cardMatchCount) {
                
                for (Card *otherCard in self.choosenCards) {
                    int score = [otherCard match:self.choosenCards];
                    if(score){
                        self.score += score * MATCH_BONUS;
                        otherCard.matched = YES;
                    }else{
                        self.score -= MISMATCH_PENALTY;
                        otherCard.choosen = NO;
                    }
                }
                
                [self.choosenCards removeAllObjects];
                if(!card.isMatched)
                    [self.choosenCards addObject:card];
            }
            self.score -= COST_TO_FLIP;
            card.choosen = YES;
        }
    }
    self.earnedScore = self.score - score;
    
    [self setBestScore:self.score];
}

- (void) setBestScore:(NSInteger)bestScore{
    if(_bestScore < bestScore){
        _bestScore = bestScore;
        [UserDefaults setBestScore:_bestScore];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:[MatchGameGame NOTYFICATION_NAME] object:self];
    }
    
}

- (Card *)cardAtIndex:(NSUInteger)index{
    
    return (index < [self.cards count])?self.cards[index]:nil;
}
@end
