//
//  main.m
//  Card Game
//
//  Created by Sure Case on 20/02/14.
//  Copyright (c) 2014 Sure Case. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
    }
}
